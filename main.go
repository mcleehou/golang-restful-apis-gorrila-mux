package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

func main()  {
	//setupDB()
	router := mux.NewRouter()

	// Route handles & endpoints

	// Get all Products
	router.HandleFunc("/products", GetAllProducts).Methods("GET")

	// Create a Product
	router.HandleFunc("/product", CreateProduct).Methods("POST")

	// Get a specific product by the id
	router.HandleFunc("/product/{id}", GetProductById).Methods("GET")

	//// Delete product by id
	router.HandleFunc("/product/{id}", DeleteProductById).Methods("DELETE")

	//// Update product by id
	router.HandleFunc("/product/{id}", UpdateProductById).Methods("PUT")

	// serve the app
	fmt.Println("Server at 8000")
	log.Fatal(http.ListenAndServe(":8000", router))

}

const (
	DB_USER     = "postgres"
	DB_PASSWORD = "root"
	DB_NAME     = "postgres"
)

// DB set up
func setupDB() *sql.DB {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", DB_USER, DB_PASSWORD, DB_NAME)
	DB, err := sql.Open("postgres", dbinfo)

	checkErr(err)

	return DB
}

func printMessage(message string) {
	fmt.Println(message)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

// GetAllProducts response and request handlers
func GetAllProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	db := setupDB()

	// Get all products from products table
	rows, err := db.Query("SELECT * FROM products")
	checkErr(err)
	var products []Product
	for rows.Next() {
		var id string
		var name string
		var price json.Number
		err = rows.Scan( &id, &name, &price)
		checkErr(err)
		products = append(products, Product{ID: id, Name: name, Price: price})
	}
	var response = JsonResponse{Type: "success", Data: products, Message: "All Products has been fetched successfully!"}
	json.NewEncoder(w).Encode(response)
}

// GetProductById response and request handlers
func GetProductById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)

	Id := params["id"]

	var response = JsonResponse{}

	if Id == "" {
		response = JsonResponse{Type: "error", Message: "You are missing productID parameter."}
	} else {
		db := setupDB()
		printMessage("Fetching product from DB")
		rows, err := db.Query("SELECT * FROM products where id = $1", Id)
		checkErr(err)
		var product []Product
		for rows.Next() {
			var id string
			var name string
			var price json.Number
			err = rows.Scan( &id, &name, &price)
			checkErr(err)
			product = append(product, Product{ID: id, Name: name, Price: price})
		}
		response = JsonResponse{Type: "success",Data: product, Message: "The product has been fetched successfully!"}
	}
	json.NewEncoder(w).Encode(response)
}

// CreateProduct response and request handlers
func CreateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var p Product
	_= json.NewDecoder(r.Body).Decode(&p)
	name := p.Name
	price := p.Price
	var response = JsonResponse{}
	if name == "" || price == "" {
		response = JsonResponse{Type: "error", Message: "You are missing productName or Price parameter."}
	} else {
		db := setupDB()
		var lastInsertID int
		err := db.QueryRow("INSERT INTO products(name, price) VALUES($1, $2) returning id;", name, price).Scan(&lastInsertID)
		checkErr(err)
		response = JsonResponse{Type: "success", Message: "The product has been inserted successfully!"}
	}
	json.NewEncoder(w).Encode(response)
}

// DeleteProductById DeleteMovie response and request handlers
func DeleteProductById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id := params["id"]
	var response = JsonResponse{}
	if id == "" {
		response = JsonResponse{Type: "error", Message: "You are missing productID parameter."}
	} else {
		db := setupDB()
		_, err := db.Exec("DELETE FROM products where id = $1", id)
		checkErr(err)
		response = JsonResponse{Type: "success", Message: "The Product has been deleted successfully!"}
	}
	json.NewEncoder(w).Encode(response)
}

// UpdateProductById DeleteProductById DeleteMovie response and request handlers
func UpdateProductById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var p Product
	_= json.NewDecoder(r.Body).Decode(&p)
	name := p.Name
	price := p.Price
	params := mux.Vars(r)
	id := params["id"]
	var response = JsonResponse{}
	if id == "" || (name == "" && price == "") {
		response = JsonResponse{Type: "error", Message: "You are missing productID parameter."}
	} else {
		db := setupDB()
		_, err := db.Exec("UPDATE products SET name=$1, price = $2 WHERE id = $3", &name, &price,  &id)
		checkErr(err)
		response = JsonResponse{Type: "success", Message: "The Product has been update successfully!"}
	}
	json.NewEncoder(w).Encode(response)
}
