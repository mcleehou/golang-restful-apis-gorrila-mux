package main

import "encoding/json"

type Product struct {
	ID  string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Price json.Number `json:"price,omitempty"`
}

type JsonResponse struct {
	Type    string `json:"type"`
	Data    []Product `json:"data"`
	Message string `json:"message"`
}


