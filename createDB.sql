create table products
(
    id    serial
        constraint products_pkey
            primary key,
    name  text                 not null,
    price numeric default 0.00 not null
);

alter table products
    owner to postgres;

